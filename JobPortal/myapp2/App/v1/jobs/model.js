var mongoose = require('mongoose');
var schema = mongoose.Schema;

var jobschema = new schema({

    
    jobname: { type: schema.Types.ObjectId ,ref:"categories"},
    jobtype: { type: String},
    description: { type: String },
    exp: { type: String },
    name: { type: String },
    qualification: { type: String },
    salary: { type: Number },
    isdelete: { type: Number, default: 1 },
    email_verification: { type: Number, default: 0 },
    status:{ type:Number,default:1 }


}, { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } });

module.exports = mongoose.model("jobs", jobschema);