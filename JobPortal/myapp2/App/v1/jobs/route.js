var express = require('express');
var mongoose = require('mongoose');

var router = express.Router();
const refUser = require("./controller");
const objUser = new refUser();
const authToken = "32bacbbe79772a6afc9cdd825f22f602";
const accountSid = "AC0cbea583c00bd9fc55d3bf008b1991ff"
var twilio = require('twilio')(accountSid, authToken)

const uservalidator = require("../user/validate");
const validation = new uservalidator();



router.get('/', async function (req, res, next) {
  objUser.getjobcategory().then(result => {
    res.render('jobform',{ data: result,user_data:req.user })
  }).catch(err => {
    res.render('jobform',{user_data:req.user,message:req.flash()})
  });
});
router.post('/', validation.jobValidate,validation.validateHandler,(req, res, next) => {
  console.log(req.body)
  objUser.addjob(req.body).then(result => {
    res.redirect('/job/joblist');
  }).catch(err => {
    console.log(err)
    // res.status(err.httpStatus || 500).send({ message: err.message, status: 0 });
  });
});
router.get('/email/:id', function (req, res, next) {
  objUser.job_email_verification(req.params.id).then((result) => {

    twilio.messages.create({
      to: "+919988681556",
      from: "+18142732113",
      body: "Your job is verified by admin "
    }).then(message => {
      console.log(message.sid)
      res.redirect('/job')
    }).catch(err => {
      res.status(err.httpStatus || 500).send({ message: err.message, status: 0 });
    });

    res.redirect('/job')
  }).catch(err => {
    res.status(err.httpStatus || 500).send({ message: err.message, status: 0 });
  });
});

router.get('/joblist', async function (req, res, next) {
  console.log(req.query)
  var data = await objUser.joblist(req.query);
  console.log("data")

  console.log(data)
  res.render('joblist', { data: data ,user_data:req.user});

});

router.get('/joblistseeker', async function (req, res, next) {
  console.log(req.query)
  var data = await objUser.joblistseeker(req.query,req.user);
  console.log("data")

  console.log(data)
  res.render('joblistseeker', { data: data,user_data:req.user });

});


router.delete('/del/:id', function (req, res, next) {

  objUser.delete_job_data(req.params.id).then(result => {
    // req.flash('success', 'User Deleted');
    res.send({ message: "Success", status: 1 });
  }).catch(err => {
    res.status(err.httpStatus || 500).send({ message: err.message, status: 0 });
  });
});

router.put('/status/:id', function (req, res, next) {
  console.log(req.params.id, req.body)
  objUser.job_status(req.params.id, req.body).then(() => {

    res.send({ status: 1 });
  }).catch(err => {
    res.status(err.httpStatus || 500).send({ message: err.message, status: 0 });
  });
});

router.get('/view/:id', function(req, res, next) {
  console.log(req.params.id)
    objUser.view_job_data(req.params.id).then(result => {
      res.send(result);
    }).catch(err => {
      res.status(err.httpStatus || 500).send({ message: err.message, status: 0 });
    });
  });


  router.get('/category', async function (req, res, next) {

    res.render('categoryform');
  });

  router.post('/category',// validation.categoryValidate,validation.validateHandler,
  (req, res, next) => {
    console.log(req.body)
    objUser.addcategory(req.body).then(result => {
      res.redirect('/job/categorylist');
    }).catch(err => {
      res.render('categorylist',{user_data:req.user})
      // res.status(err.httpStatus || 500).send({ message: err.message, status: 0 });
    });
  });

  router.get('/categorylist', async function (req, res, next) {
    console.log(req.query)
    var data = await objUser.categorylist(req.query);
    console.log("data")
  
    console.log(data)
    res.render('categorylist', { data: data ,user_data:req.user});
  
  });


  router.get('/category/edit/:id', function(req, res, next) {
    console.log(req.params.id)
    let id=mongoose.Types.ObjectId(req.params.id)
      objUser.get_category_data(id).then(result => {
        res.render('editCategoryForm', { data:result,user_data:req.user});
      }).catch(err => {
        res.status(err.httpStatus || 500).send({ message: err.message, status: 0 });
      });
    });
    

  router.post('/category/edit/:id', function(req, res, next) {
    console.log(req.params.id)
    
      objUser.update_category_data(req.params.id,req.body).then(result => {
        console.log(result);
        res.redirect('/job/categorylist');
      }).catch(err => {
        res.status(err.httpStatus || 500).send({ message: err.message, status: 0 });
    });
   });


  router.delete('/category/del/:id', function (req, res, next) {

    objUser.delete_category_data(req.params.id).then(result => {
      // req.flash('success', 'User Deleted');
      res.send({ message: "Success", status: 1 });
    }).catch(err => {
      res.status(err.httpStatus || 500).send({ message: err.message, status: 0 });
    });
  });
module.exports = router;