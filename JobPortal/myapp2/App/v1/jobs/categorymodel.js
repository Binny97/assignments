var mongoose = require('mongoose');
var schema = mongoose.Schema;

var categoryschema = new schema({

    
    category: { type: String },
    subcategory: [{ type: String }],
    isdelete: { type: Number, default: 1 },
   


}, { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } });

module.exports = mongoose.model("category", categoryschema);

// category:String,
// subcategory:[{
//     type:String,
//     model:[{
//         type:String,
//         colour:[{
//             name:String,
//             image:String
//         }],
//         size:[{
//             val:Number,
//             price:Number
//         }]
//     }]
// }],