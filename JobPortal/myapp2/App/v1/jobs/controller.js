var mongoose = require('mongoose');
const job = require("./model.js");
const user = require("../user/model");

const category = require("./categorymodel.js");
const nodemailer = require('nodemailer');
var passwordHash = require('password-hash')

const ObjectId =mongoose.Types.ObjectId;


class User {
   async getjobcategory(data) {
      try {
         // console.log(data)
         // let find_data = await user.findOne({ email: data.email });
         // if (!find_data) {
         let result = await category.find();
         //   var data =[];
         //   for(var i=0; i < result.length; i++) {
         //    data.push(result[i].subcategory.split(',').map(function (val) { return val }));
         //   }
         //  let y =  data.map((ele) => ele.map((elem) => {
         //    return elem;
         //   }));
         //   let x = result[3].subcategory
         //   "node js , python , php ,el block chain"
         //   let y = x.
         //   console.log(y)
         return Promise.resolve(result);
         // }
         // else {
         // return Promise.reject();
         // }
      } catch (error) {
         console.log(error)

      }
      // let x = "node js , python , php , block chain"
      // undefined
      // let y = x.split(',') 
      // undefined
      // console.log(y)
      // VM2741:1 (4) ["node js ", " python ", " php ", " block chain"]
   }
   async addjob(data) {
      try {
         // console.log(data)
         // let find_data = await user.findOne({ email: data.email });
         // if (!find_data) {
         let result = await job.create(data);
         let transporter = nodemailer.createTransport({
            host: "smtp.gmail.com",
            port: 587,
            secure: false, // true for 465, false for other ports
            auth: {
               user: "sainibinny1697@gmail.com", // generated ethereal user
               pass: "abhibinny" // generated ethereal password
            }
         });

         let info = await transporter.sendMail({
            to: "sainibinny1697@gmail.com", // list of receivers
            subject: "New Job Added", // Subject line
            text: "Email" + result.jobname + "\n" + "Name" + result.jobtype,
            html: '<div>Email:' + result.jobname + '<br> Name:' + result.jobtype + ' <br> Please<a href="http://localhost:3000/job/email/' + result.id + '"> click here</a>to verify</div>'
            // html:'<a href="http://localhost:3000/email/'+data1._id+'"> click here</a>'
         });


         console.log("Message sent: %s", info.messageId);

         console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));



         return Promise.resolve();
         // }
         // else {
         // return Promise.reject();
         // }
      } catch (error) {
         console.log(error)

      }

   }
   async job_email_verification(id) {
      try {

         let data = await job.update({ _id: id }, { $set: { email_verification: 1 } });

         return Promise.resolve(data);
      } catch (error) {
         console.log(error)

      }
   }
   async joblist(data) {
      try {
         console.log(data, "heloooo")
         let pageNo = 1, size = 5;
         if (data.page) {
            pageNo = parseInt(data.page)
         }
         let skip = ((pageNo - 1) * size)
         // let query = { isdelete: 1, email_verification: 1 }
         let userCount = await job.find({ isdelete: 1, email_verification: 1 }).count();
         let totalPages = parseInt(userCount / size)
         if (userCount % size != 0) {
            totalPages++;
         }

         // let query =[
         //    {
         //      "$project": {
               
         //       "category":1,
         //       "subcategory":1, 
         //       "_id": {
         //          "$toString": "$_id"
         //        },
         //      }
         //    },
         //    {
         //      "$lookup": {
         //        "from": "jobs",
         //        "localField": "_id",
         //        "foreignField": "jobname",
         //        "as": "role"
         //      }
         //    },
         //    {
         //  "$project": {
               
         //        "role.name":1,

         //       category:1,
         //       subcategory:1,
         //    }
         //    }
            
         //  ]          
          
         // let query = [{$lookup:{from:'categories',localField:'jobname',foreignField:ObjectId('_id'),as:'form'}},
         // {$project:{jobname:1,jobtype:1,category:1,subcategory:1}}]
      
//          db.jobs.aggregate({$lookup:{from:'categories',localField:'jobname',foreignField:'_id',as:'form'}},
// {$project:{jobname:1,jobtype:1,category:1,subcategory:1}}).pretty();
// let result = await category.aggregate([

// {
//    $match:{_id:ObjectId("5e2144454c29b61a2e76ad78")}
// }
// ])

// const ObjectId = mongoose.Types.ObjectId;
// const User = mongoose.model('User')

// User.aggregate([
// {
// $match: { _id: ObjectId('560c24b853b558856ef193a3') }
// }
// ])
// console.log(result)
         // let result = await company.find();
         // let result = await category.aggregate(query);
         let result = await job.aggregate([
{$match:{status:1}},
{$lookup:{
   "from": "categories",
   "localField": "jobname",
   "foreignField": "_id",
   "as": "category_id"
}}
         ]);
         const util = require('util')
         console.log(util.inspect(result, {depth:null}))
         
// console.log(util.inspect(query, false, null, true))

// let data = await user_detailes.aggregate(query);
// console.log(util.inspect(result, false, null, true ))
// console.log(result)
         result.current = pageNo
         if (pageNo == 1) {
            result.prev = 1
         }
         else {
            result.prev = pageNo - 1
         }
         result.next = pageNo + 1
         result.last = totalPages
         result.count = userCount
         result.sr = (pageNo - 1) * size + 1
         return Promise.resolve(result);

      } catch (error) {
         console.log(error)

      }

   }
   async joblistseeker(data,user_data) {
      try {
         console.log(data, "heloooo")
         let pageNo = 1, size = 5;
         if (data.page) {
            pageNo = parseInt(data.page)
         }
         let skip = ((pageNo - 1) * size)
         
         let query = [
            {$match:{'jobtype':user_data.apply}},
            { $lookup:
            { from: 'users',
            let: { local: '$jobtype' },
            pipeline: [{ $match: { $expr: { $eq: ['$jobtype', '$$local'] } } }],
            as: 'result' } }, { $skip: skip }, { $limit: size },
            
            { $project:
            { _id:1,name:1,jobtype:1,jobname:1,created_at:1,'result.jobsapplied':1, 'result.apply': 1 } }
            ];

         // let query = [{ $lookup: { from: 'seekers', localField: 'jobname', foreignField: 'apply', as: 'result' } },//{$skip:skip},{$limit:size},
         // {$project:{_id:1,name:1,jobtype:1,jobname:1,created_at:1,'result.apply':1}}]
         let result = await job.aggregate(query);
          console.log(result)
         let userCount = await job.find().count();
         let totalPages = parseInt(userCount / size)
         if (userCount % size != 0) {
            totalPages++;
         }

         // let result = await company.find();
         // let result = await job.aggregate(query);

         result.current = pageNo
         if (pageNo == 1) {
            result.prev = 1
         }
         else {
            result.prev = pageNo - 1
         }
         result.next = pageNo + 1
         result.last = totalPages
         result.count = userCount
         result.sr = (pageNo - 1) * size + 1
         console.log(result)
         return Promise.resolve(result);

      } catch (error) {
         console.log(error)

      }

   }

   async delete_job_data(id) {
      try {


         let data = await job.updateOne({ _id: id }, { isdelete: 0 });
         return Promise.resolve(data);
      } catch (error) {
         console.log(error)
      }


   }

   async job_status(id, user_data) {
      try {

         let data = await job.update({ _id: mongoose.Types.ObjectId(id) }, user_data);

         return Promise.resolve();
      } catch (error) {
         console.log(error)

      }
   }

   async view_job_data(id) {
      try {
         let data = await job.findOne({ _id: id });
         console.log(data)
         return Promise.resolve(data);
      } catch (error) {
         console.log(error)
      }
   }

   async addcategory(data) {
      try {
         // console.log(data)
         // let find_data = await user.findOne({ email: data.email });
         // if (!find_data) {
         let result = await category.create(data);


         return Promise.resolve();
         // }
         // else {
         // return Promise.reject();
         // }
      } catch (error) {
         console.log(error)

      }

   }

   async categorylist(data) {
      try {
         console.log(data, "heloooo")
         let pageNo = 1, size = 5;
         if (data.page) {
            pageNo = parseInt(data.page)
         }
         let skip = ((pageNo - 1) * size)
         let query = { isdelete: 1 }
         let userCount = await category.find(query).count();
         let totalPages = parseInt(userCount / size)
         if (userCount % size != 0) {
            totalPages++;
         }

         // let result = await company.find();
         let result = await category.find(query).skip(skip).limit(size);

         result.current = pageNo
         if (pageNo == 1) {
            result.prev = 1
         }
         else {
            result.prev = pageNo - 1
         }
         result.next = pageNo + 1
         result.last = totalPages
         result.count = userCount
         result.sr = (pageNo - 1) * size + 1
         return Promise.resolve(result);

      } catch (error) {
         console.log(error)

      }

   }

   async delete_category_data(id) {
      try {


         let data = await category.updateOne({ _id: id }, { isdelete: 0 });
         return Promise.resolve(data);
      } catch (error) {
         console.log(error)
      }


   }



   async get_category_data(id) {
      try {
         let data = await category.findOne({ _id: id });
         return Promise.resolve(data);

      } catch (error) {
         console.log(error)

      }


   }

   async update_category_data(id, user_data) {
      try {

         let data = await category.updateOne({ _id: id }, user_data);

         return Promise.resolve(data);
      } catch (error) {
         console.log(error)

      }


   }
}




module.exports = User;