var mongoose = require('mongoose');
var schema = mongoose.Schema;

var subcategoryschema = new schema({


    category: { type: String },
    subcategory: [
        {
            name:
                { type: String }
        }
    ],
    isdelete: { type: Number, default: 1 },



}, { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } });

module.exports = mongoose.model("subcategory", subcategoryschema);