const { check, validationResult, query } = require('express-validator');
class UserValidator {
    validateHandler(req, res, next) {
        let errors = validationResult(req);
        if (errors.isEmpty()) {
            next();
        } else {
            res.status(400).send({ message: "Validation Errors", status: 0, errors: errors });
        }
    };
    get loginValidate() {
            return [
                check('username').not().isEmpty().withMessage("Please enter email"),
                check('password').not().isEmpty().withMessage("Please enter password")
            ]
        }
        get seekerValidate() {
            return [
                check('name').not().isEmpty().withMessage("Please enter name"),
                check('email').not().isEmpty().withMessage("Please enter email"),
                check('password').not().isEmpty().withMessage("Please enter password"),
                check('number').not().isEmpty().withMessage("Please enter number"),
                check('address').not().isEmpty().withMessage("Please enter address"),
                check('matriculation.schoolName').not().isEmpty().withMessage("Please enter schoolName"),
                check('matriculation.marks').not().isEmpty().withMessage("Please enter marks"),

                check('sersecondary.schoolName').not().isEmpty().withMessage("Please enter schoolName"),
                check('sersecondary.marks').not().isEmpty().withMessage("Please enter marks"),

                check('graduation.clgName').not().isEmpty().withMessage("Please enter clgName"),
                check('graduation.marks').not().isEmpty().withMessage("Please enter marks"),

                check('postGraduation.clgName').not().isEmpty().withMessage("Please enter clgName"),
                check('postGraduation.marks').not().isEmpty().withMessage("Please enter marks"),

                check('apply').not().isEmpty().withMessage("Please enter the stream in which you want to apply"),
                check('exp.companyName').not().isEmpty().withMessage("Please enter companyName"),
                check('exp.years').not().isEmpty().withMessage("Please enter years")

            ]
        }

        get companyValidate() {
            return [
                check('name').not().isEmpty().withMessage("Please enter name"),
                check('email').not().isEmpty().withMessage("Please enter email"),
                check('password').not().isEmpty().withMessage("Please enter password"),
            ]
        }


        get categoryValidate() {
            return [
                check('category').not().isEmpty().withMessage("Please enter category"),
                check('subcategory').not().isEmpty().withMessage("Please enter subcategory")
            ]
        }

        get jobValidate() {
            return [
                check('description').not().isEmpty().withMessage("Please enter description"),
                check('exp').not().isEmpty().withMessage("Please enter exp"),
                check('name').not().isEmpty().withMessage("Please enter name"),
                check('qualification').not().isEmpty().withMessage("Please enter qualification"),

                check('salary').not().isEmpty().withMessage("Please enter salary"),

            ]
        }

}

module.exports = UserValidator;