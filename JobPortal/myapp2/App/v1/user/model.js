var mongoose = require('mongoose');
var schema = mongoose.Schema;

var seekerschema = new schema({

    //Basic seeker information
    name: { type: String },
    email: { type: String },
    dob: { type: String },
    password: { type: String },
    phnumber: { type: String },
    address: { type: String },
    city: { type: String },
    state: { type: String },
    country: { type: String },
    pin: { type: String },

    //Seeker qualification
    matriculation: {
        schoolName: { type: String },
        marks: { type: Number }
    },
    sersecondary: {
        schoolName: { type: String },
        marks: { type: Number }
    },
    graduation: {
        clgName: { type: String },
        marks: { type: Number }
    },
    postGraduation: {
        clgName: { type: String },
        marks: { type: Number }
    },

    //seeeker experience
    exp: {
        companyName: { type: String },
        years: { type: Number }
    },
    apply: {
        type: String 
    },


    isdelete: { type: Number, default: 1 },
    email_verification: { type: Number, default: 0 },
    profile_pic: { type: String },
    profile_pic_thumb: { type: String },
    status:{ type:Number,default:1 },
    jobsapplied:{ type:Number,default:0 },
    role:{ type:String, default:'admin'},
    cmplregistration:{ type:Number, default:0},


}, { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } });

module.exports = mongoose.model("user", seekerschema);
