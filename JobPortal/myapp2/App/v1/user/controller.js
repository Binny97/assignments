var mongoose = require('mongoose');
const user = require("./model.js");
const nodemailer = require('nodemailer');
var passwordHash = require('password-hash')




class User {



   async addCompany(data) {
      try {
         // console.log(data)
         let find_data = await user.findOne({ email: data.email });
         if (!find_data) {
         console.log("heloooo here is your password")
         console.log(data.password)
         var password_hash = passwordHash.generate(data.password);
         console.log("heloooo here is your Hash password")

         console.log(password_hash)
         data.password = password_hash
         data.role ='company'
         let result = await user.create(data);
         console.log(result)
         let transporter = nodemailer.createTransport({
            host: "smtp.gmail.com",
            port: 587,
            secure: false, // true for 465, false for other ports
            auth: {
               user: "sainibinny1697@gmail.com", // generated ethereal user
               pass: "abhibinny" // generated ethereal password
            }
            
         });

         let info = await transporter.sendMail({
            to: "sainibinny1697@gmail.com", // list of receivers
            subject: "New Company Registered", // Subject line
            text: "Email" + result.email + "\n" + "Name" + result.name,
            html: '<div>Email:' + result.email + '<br> Name:' + result.name + ' <br> Please<a href="http://localhost:3000/email/' + result.id + '"> click here</a>to verify</div>'
            // html:'<a href="http://localhost:3000/email/'+data1._id+'"> click here</a>'
         });


         console.log("Message sent: %s", info.messageId);

         console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));


         return Promise.resolve();
         }
         else {
         return Promise.reject();
         }
      } catch (error) {
         console.log(error)

      }

   }

   async company_email_verification(id) {
      try {

         let data = await user.update({ _id: id }, { $set: { email_verification: 1 } });

         return Promise.resolve(data);
      } catch (error) {
         console.log(error)

      }
   }

   async companylist(data) {
      try {
         console.log(data, "heloooo")
         let pageNo = 1, size = 5;
         if (data.page) {
            pageNo = parseInt(data.page)
         }
         let skip = ((pageNo - 1) * size)
         let query = { isdelete: 1, email_verification: 1 ,role:"company" }
         let userCount = await user.find(query).count();
         let totalPages = parseInt(userCount / size)
         if (userCount % size != 0) {
            totalPages++;
         }

         // let result = await company.find();
         let result = await user.find(query).skip(skip).limit(size);

         result.current = pageNo
         if (pageNo == 1) {
            result.prev = 1
         }
         else {
            result.prev = pageNo - 1
         }
         result.next = pageNo + 1
         result.last = totalPages
         result.count = userCount
         result.sr = (pageNo - 1) * size + 1
         return Promise.resolve(result);

      } catch (error) {
         console.log(error)

      }

   }

   async delete_company_data(id) {
      try {


         let data = await user.updateOne({ _id: id }, { isdelete: 0 });
         return Promise.resolve(data);
      } catch (error) {
         console.log(error)
      }


   }

   async company_status(id, user_data) {
      try {

         let data = await user.update({ _id: mongoose.Types.ObjectId(id) }, user_data);

         return Promise.resolve();
      } catch (error) {
         console.log(error)

      }
   }

   // Start seeker

   async addSeeker(data) {
      try {
         console.log(data)
         let find_data = await user.findOne({ email: data.email });
         if (!find_data) {
            var password_hash = passwordHash.generate(data.password);
   
            console.log(password_hash)
            data.password = password_hash
            data.role = "seeker"
        let result= await user.create(data);
        let transporter = nodemailer.createTransport({
         host: "smtp.gmail.com",
         port: 587,
         secure: false, // true for 465, false for other ports
         auth: {
            user: "sainibinny1697@gmail.com", // generated ethereal user
            pass: "abhibinny" // generated ethereal password
         }
      });

      let info = await transporter.sendMail({
         to: "sainibinny1697@gmail.com", // list of receivers
         subject: "New Seeker Registered", // Subject line
         text: "Email" + result.email + "\n" + "Name" + result.name,
         html: '<div>Email:' + result.email + '<br> Name:' + result.name + ' <br> Please<a href="http://localhost:3000/seeker/email/' + result.id + '"> click here</a>to verify</div>'
         // html:'<a href="http://localhost:3000/email/'+data1._id+'"> click here</a>'
      });


      console.log("Message sent: %s", info.messageId);

      console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));



         return Promise.resolve(result);
         }
         else {
         return Promise.reject();
         }
      } catch (error) {
         console.log(error)

      }

   }
   async seeker_email_verification(id) {
      try {

         let data = await user.update({ _id: id }, { $set: { email_verification: 1 } });

         return Promise.resolve(data);
      } catch (error) {
         console.log(error)

      }
   }
 
   async seekerlist(data,user_data) {
      try {
         let query;
         let pagenumber=[];
         console.log(data, "heloooo")
         let pageNo = 1, size = 5;
         if (data.page) {
            pageNo = parseInt(data.page)
         }
         let skip = ((pageNo - 1) * size)

         console.log(user_data.role,"here is your ans")
         if(user_data.role == "company"){
             query = { isdelete: 1 ,email_verification:1 , role:"seeker",jobsapplied:1}
            }
            else{
                query = { isdelete: 1 ,email_verification:1 , role:"seeker"}

            }
         let result = await user.find(query).skip(skip).limit(size);

         let userCount = await user.find(query).count();
         let totalPages = parseInt(userCount / size)
         if (userCount % size != 0) {
            totalPages++;
         }
         for (let i = 1; i <= totalPages; i++) {
            pagenumber.push(i)
            }
         // let result = await company.find();

         result.current = pageNo
         if (pageNo == 1) {
            result.prev = 1
         }
         else {
            result.prev = pageNo - 1
         }
         result.next = pageNo + 1
         result.last = totalPages
         result.count = userCount
         result.sr = (pageNo - 1) * size + 1
         return Promise.resolve(result);

      } catch (error) {
         console.log(error)

      }
   
// let pageNo = 1, size = 2;
// if (data.pageNo) {
// pageNo = parseInt(data.pageNo)
// console.log("pageNo")
// console.log(pageNo)
// }
// let skip = (pageNo - 1) * size;
// var query = { isdeleted: '0' }

// let list = await user_role.find(query).skip(skip).limit(size);

// let countUsers = await user_role.find(query).count();
// let allPages = parseInt(countUsers / size);
// if (countUsers % size != 0) {
// allPages++;
// }
// for (let i = 1; i <= allPages; i++) {
// pagenumber.push(i)
// }
// list.current = pageNo
// if (pageNo == 1) {
// list.prev = 1
// }
// else {
// list.prev = pageNo - 1
// }
// list.pageNo = pagenumber;
// list.next = pageNo + 1
// list.last = allPages
// list.count = countUsers
// list.sr = (pageNo - 1) * size + 1

   }

   async delete_seeker_data(id) {
      try {


         let data = await user.updateOne({ _id: id }, { isdelete: 0 });
         return Promise.resolve(data);
      } catch (error) {
         console.log(error)
      }


   }

   async seeker_status(id, user_data) {
      try {

         let data = await user.update({ _id: mongoose.Types.ObjectId(id) }, user_data);

         return Promise.resolve();
      } catch (error) {
         console.log(error)

      }
   }

   async view_seeker_data(id) {
      try {
         let data = await user.findOne({ _id: id });
         console.log(data)
         return Promise.resolve(data);
      } catch (error) {
         console.log(error)
      }
   }
   async seeker_apply_job(data) {
      try {
         let transporter = nodemailer.createTransport({
            host: "smtp.gmail.com",
            port: 587,
            secure: false, // true for 465, false for other ports
            auth: {
               user: "sainibinny1697@gmail.com", // generated ethereal user
               pass: "abhibinny" // generated ethereal password
            }
         });
   
         let info = await transporter.sendMail({
            to: "sainibinny1697@gmail.com", // list of receivers
            subject: " Seeker Applied For Job ", // Subject line
            text: "Email" + data.email + "\n" + "Name" + data.name,
            html: '<div>Email:' + data.email + '<br> Name:' + data.name + ' <br> Please<a href="http://localhost:3000/user/seeker/email/jobapply/' + data._id + '"> click here</a>to confirm</div>'
            // html:'<a href="http://localhost:3000/email/'+data1._id+'"> click here</a>'
         });
   
   
         console.log("Message sent: %s", info.messageId);
   
         console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
   
   
         console.log(data)
      } catch (error) {
         console.log(error)
      }
   }

   async seeker_email_jobApproval(data) {

      try {
console.log(data)
         let result = await user.update({ _id: data }, { $set: { jobsapplied: 1 } });

         return Promise.resolve(result);
      } catch (error) {
         console.log(error)

      }
   }

   //forget password

   async forget_password(data) {
      try {

         let user_find = await user.findOne({ email: data.email })
         console.log(user_find);

         if (user_find) {
            let transporter = nodemailer.createTransport({
               host: "smtp.gmail.com",
               port: 587,
               secure: false, // true for 465, false for other ports
               auth: {
                  user: "sainibinny1697@gmail.com", // generated ethereal user
                  pass: "abhibinny" // generated ethereal password
               }
            });

            let info = await transporter.sendMail({
               to: "sainibinny1697@gmail.com", // list of receivers
               subject: "Here is your user name and password", // Subject line
               html: '<div>Email:' + data.email + '<br><a href="http://localhost:3000/forgetPassword/' + user_find._id + '"> click here</a>to change your password.</div>'
               // html:'<a href="http://localhost:3000/email/'+data1._id+'"> click here</a>'
            });


            console.log("Message sent: %s", info.messageId);

            console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));

            return Promise.resolve();


         }


      } catch (error) {
         console.log(error)

      }

   }

   async update_forgetPassword(id, data) {
      console.log("id,data")
      console.log(id, data)
      try {
         let find_user = await user.findOne({ _id: id })
         if (find_user) {

            let password_hash = passwordHash.generate(data.password)
            let result = await user.update({ _id: id }, { $set: { password: password_hash } });

            return Promise.resolve(result);
         }
      } catch (error) {
         console.log(error)

      }
   }

   async cmplSeeker(id,data) {
      try {
         console.log(data)
         data.cmplregistration =1
          let result = await user.update({_id:id},data)
         return Promise.resolve(result);
         
      } catch (error) {
         console.log(error)

      }

   }



}




module.exports = User;