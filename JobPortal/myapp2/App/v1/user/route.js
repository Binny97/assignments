var express = require('express');
var router = express.Router();
const refUser = require("./controller");
const objUser = new refUser();

const authToken = "32bacbbe79772a6afc9cdd825f22f602";
const accountSid = "AC0cbea583c00bd9fc55d3bf008b1991ff"
var twilio = require('twilio')(accountSid, authToken)


// const acl = require("./App/v1/middlewear/acl");
// const Acl = new acl();


//company listing
router.get('/companylist', async function (req, res, next) {
  console.log(req.query)
  var data = await objUser.companylist(req.query);
  console.log("data")

  console.log(data)
  res.render('companyList', { data: data ,message:req.flash(),user_data:req.user});

});


// Delete Action on company listing
router.delete('/del/:id', function (req, res, next) {

  objUser.delete_company_data(req.params.id).then(result => {
    // req.flash('success', 'User Deleted');
    res.send({ message: "Success", status: 1 });
  }).catch(err => {
    res.status(err.httpStatus || 500).send({ message: err.message, status: 0 });
  });
});

// Status of company 
router.put('/status/:id', function (req, res, next) {
  console.log(req.params.id, req.body)
  objUser.company_status(req.params.id, req.body).then(() => {

    res.send({ status: 1 });
  }).catch(err => {
    res.status(err.httpStatus || 500).send({ message: err.message, status: 0 });
  });
});


//seeker listing
router.get('/seekerlist', async function (req, res, next) {
  // console.log(req.user,'asdfghjkjhgfdfghjhgfdxc vbnhygtfdcxvbhj')
  console.log(req.query)
  var data = await objUser.seekerlist(req.query,req.user);
  console.log("data")

  console.log(data)
  res.render('seekerList', { data: data ,user_data:req.user});

});

// Delete Action on seeker listing
router.delete('/seeker/del/:id', function (req, res, next) {

  objUser.delete_seeker_data(req.params.id).then(result => {
    // req.flash('success', 'User Deleted');
    res.send({ message: "Success", status: 1 });
  }).catch(err => {
    res.status(err.httpStatus || 500).send({ message: err.message, status: 0 });
  });
});

// Status of seeker 
router.put('/seeker/status/:id', function (req, res, next) {
  console.log(req.params.id, req.body)
  objUser.seeker_status(req.params.id, req.body).then(() => {

    res.send({ status: 200 });
  }).catch(err => {
    res.status(err.httpStatus || 500).send({ message: err.message, status: 0 });
  });
});

//view Seeker data
router.get('/seeker/view/:id', function(req, res, next) {
  console.log(req.params.id)
    objUser.view_seeker_data(req.params.id).then(result => {
      res.send(result);
    }).catch(err => {
      res.status(err.httpStatus || 500).send({ message: err.message, status: 0 });
    });
  });

  //seeker Apply for job
  router.get('/seeker/apply', function(req, res, next) {
      objUser.seeker_apply_job(req.user).then(result => {
        res.redirect('/job/joblist')
      }).catch(err => {
        res.status(err.httpStatus || 500).send({ message: err.message, status: 0 });
      });
    });

    router.get('/seeker/email/jobapply/:id', function (req, res, next) {
      objUser.seeker_email_jobApproval(req.params.id).then((result) => {
    
        twilio.messages.create({
          to: "+919988681556",
          from: "+18142732113",
          body: "Company accept your job application  ."
        }).then(message => {
          console.log(message.sid)
          res.redirect('/user')
        }).catch(err => {
          res.status(err.httpStatus || 500).send({ message: err.message, status: 0 });
        });
    
        res.redirect('/')
      }).catch(err => {
        res.status(err.httpStatus || 500).send({ message: err.message, status: 0 });
      });
    });

//complete seeker registration

    router.get('/cmplseeker', async function (req, res, next) {

      res.render('completeSeekerRegistration',{data:req.user,message:req.flash(),user_data:req.user});
    });

    router.post('/cmplseeker',
 (req, res, next) => {
  console.log(req.user._id)
  objUser.cmplSeeker(req.user._id,req.body).then(result => {
    console.log(result)
    res.redirect('/job/joblistseeker');
  }).catch(err => {
    console.log("error credentials exist")
    res.redirect('/')
    res.status(err.httpStatus || 500).send({ message: err.message, status: 0 });
  });
});

module.exports = router;