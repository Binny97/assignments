var express = require('express');
var router = express.Router();
const user = require("./model.js");
const refUser = require("./controller");
const objUser = new refUser();

const authToken = "32bacbbe79772a6afc9cdd825f22f602";
const accountSid = "AC0cbea583c00bd9fc55d3bf008b1991ff"
var twilio = require('twilio')(accountSid, authToken)


var passport = require("passport");
var LocalStrategy = require("passport-local").Strategy;

var passwordHash = require('password-hash')

const uservalidator = require("./validate");
const validation = new uservalidator();

var abc;



router.get('/', async function (req, res, next) {

  res.render('landing',{message:req.flash(),user_data:req.user});
});

//company registration 
router.get('/company', async function (req, res, next) {

  res.render('companyRegistrationForm',{user_data:req.user});
});

router.post('/company',validation.companyValidate,validation.validateHandler,(req, res, next) => {
  console.log(req.body)
  objUser.addCompany(req.body).then(result => {
    req.flash('success', 'Welcome');
    res.redirect('/job');
  }).catch(err => {
    res.redirect('/')
    res.status(err.httpStatus || 500).send({ message: err.message, status: 0 });
  });
});

// email verification of company
router.get('/email/:id', function (req, res, next) {
  objUser.company_email_verification(req.params.id).then((result) => {

    twilio.messages.create({
      to: "+919988681556",
      from: "+18142732113",
      body: "Your Account is verified by admin , now you can post jobs ."
    }).then(message => {
      console.log(message.sid)
      res.redirect('/user')
    }).catch(err => {
      res.status(err.httpStatus || 500).send({ message: err.message, status: 0 });
    });

    res.redirect('/')
  }).catch(err => {
    res.status(err.httpStatus || 500).send({ message: err.message, status: 0 });
  });
});

//seekers registration 
router.get('/seeker', async function (req, res, next) {

  res.render('seeker_registration_form',{message:req.flash(),user_data:req.user});
});
router.post('/seeker',//validation.seekerValidate,validation.validateHandler,
 (req, res, next) => {
  console.log(req.body)
  objUser.addSeeker(req.body).then(result => {
    req.flash('success', 'Successfully registered , Wait for admin to approve');
    res.redirect('/');
  }).catch(err => {
    req.flash('error', 'Email exist , choose another one');

    res.redirect('/')
    res.status(err.httpStatus || 500).send({ message: err.message, status: 0 });
  });
});


//seeker Email verification
router.get('/seeker/email/:id', function (req, res, next) {
  objUser.seeker_email_verification(req.params.id).then((result) => {

    twilio.messages.create({
      to: "+919988681556",
      from: "+18142732113",
      body: "Your Account is verified by admin , now you can Apply jobs ."
    }).then(message => {
      console.log(message.sid)
      res.redirect('/')
    }).catch(err => {
      res.status(err.httpStatus || 500).send({ message: err.message, status: 0 });
    });

    res.redirect('/')
  }).catch(err => {
    res.status(err.httpStatus || 500).send({ message: err.message, status: 0 });
  });
});



//login page
router.get('/login', async function (req, res, next) {

  res.render('login_layout',{user_data:req.user});
});


router.post('/login',validation.loginValidate,validation.validateHandler, passport.authenticate('local', {
  failuerRedirect:'/',


}),(req,res,next) =>{
  try {
         
    passport.authenticate('local', async (err, user) => {
       if (err) {
          res.redirect("/");
       }
      
       if (!user) {
          // req.flash('error', 'wrong input , check your credentials');
          res.redirect("/");
       }
      else if(user.role == "admin" && user.email_verification == 1){
        req.flash('success', 'Welcome');

        res.redirect('/dashboard');
       }
       else if((user.role == "company" && user.email_verification == 1)&&(user.status ==1)){
        req.flash('success', 'Welcome');

        res.redirect('/job');

       }
       else if((user.role == "seeker" && user.email_verification == 1)&&(user.cmplregistration == 0 && user.status ==1)){
        req.flash('success', 'Welcome');

        res.redirect('/user/cmplseeker');

       }
       else if((user.role == "seeker" && user.email_verification == 1)&&(user.cmplregistration == 1  && user.status ==1)){
        req.flash('success', 'Welcome');

        res.redirect('/job/joblistseeker');

       }
       else{
         req.flash('error', 'Wrong input');
          res.redirect('/');
       }
       
    }
    )(req, res, next);
 } catch (err) {
    console.log("erorr in login");
    res.status(400).send({ message: err.message, status: 0 });
 }
 console.log(req.user,"req.user data >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>..")
});

passport.use(new LocalStrategy({ usernameField: 'username', passwordField: 'password', passReqToCallback: true },
   async (req,username, password, done) => {
      try{
        let data = await user.findOne({ email: username });
         
        if (!data) {
           return done(null, false);
        }
        if (!passwordHash.verify(password, data.password)) {
           return done(null, false);
        }

        return done(null, data)
      }catch(err){
         console.log("error in passport");
         console.log(err);
      }
      

   }));

   passport.serializeUser(function (user, done) {
    done(null, user._id);
 });
 
 passport.deserializeUser( async function (id, done) {
    // users.findById(userId, (err, user) => done(err, user));
    let user_data = await user.findOne({_id: id});
    user_data = JSON.parse(JSON.stringify(user_data));
    done(null,user_data );
 });


 // Forget password
 router.post('/forgetPassword', (req, res, next) => {

  objUser.forget_password(req.body).then(result => {

    res.redirect('/');

  }).catch(err => {
    res.status(err.httpStatus || 500).send({ message: err.message, status: 0 });
  });
});

router.get('/form', async function (req, res, next) {

  res.render('forgetPassword_form',{user_data:req.user});
});

router.get('/forgetPassword/:id', (req,res) => {
   console.log(req.params.id)

   abc = req.params.id;
   console.log("here is the user id")
   console.log(abc)

   res.redirect('/form')
  
});

router.post('/forgetPasswordUpdate', (req,res) => {
   console.log(abc);

  objUser.update_forgetPassword(abc,req.body).then(result => {

     res.redirect('/');
 
   }).catch(err => {
     res.status(err.httpStatus || 500).send({ message: err.message, status: 0 });
   });
});


module.exports = router;