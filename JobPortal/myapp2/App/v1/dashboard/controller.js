var mongoose = require('mongoose');
const user = require("../user/model");
var passwordHash = require('password-hash')

class dashboard {



   async countRecord(){
      let companydata = await user.count({"isdelete" : 1,role:"company"});
      let seekerdata = await user.count({"isdelete" : 1,role:"seeker"});
      let data ={
         companydata,seekerdata
      }

      return Promise.resolve(data);
      }
      
      
   
      async changePassword(user_data,data) {

         try {
            let verify= passwordHash.verify(data.OldPassword,user_data.password)
            if(verify){

            let password_hash = passwordHash.generate(data.NewPassword)
            console.log(password_hash)
         let result = await user.update({ _id: user_data._id }, { $set: { password: password_hash } });
         return Promise.resolve(result);
            }

         } catch (err) {
             return Promise.reject({ message: err.message, httpStatus: 400 })
         }
     }


     


      async updateProfile(user_data,data){
         try {
            let result = await user.updateOne({ _id:user_data._id },data);
   
            return Promise.resolve(result);
         } catch (error) {
            console.log(error)
   
         }
         }
     


      async uploadProfileImage(user_data,url){
         try {
         
               let result = await user.updateOne({ _id:user_data._id },{profile_pic:url.url});
   
               return Promise.resolve(result);
            
         } catch (error) {
            console.log(error)
   
         }
         }

}




module.exports = dashboard;