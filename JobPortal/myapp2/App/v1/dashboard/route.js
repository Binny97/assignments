var express = require('express');
var router = express.Router();
const refUser = require("./controller");
const objUser = new refUser();
var abc;

router.get('/', (req, res) => {
   objUser.countRecord().then((result) => {
      res.render('tiles', { active: 'dashboard', count: result,user_data:req.user })
   });
});

router.get('/changeProfile', (req, res) => {
   res.render('changeProfile', { data: req.user })

});

router.post('/changeProfile', (req, res) => {
   console.log("sdfghjmk")
   console.log(req.body)
   objUser.updateProfile(req.user, req.body).then(result => {

      console.log(result)
      res.redirect("/dashboard")
   }).catch(err => {
      //error handling
      res.status(err.httpStatus || 500).send({ message: err.message, status: 0 });
   });
});

router.post('/profileImage', (req, res) => {
   console.log(req.body)
   objUser.uploadProfileImage(req.user, req.body).then(result => {

      //  console.log(result)
      res.redirect("/dashboard")
   }).catch(err => {
      //error handling
      res.status(err.httpStatus || 500).send({ message: err.message, status: 0 });
   });
});

router.post('/changePasswordUpdate', (req, res, next) => {
   objUser.changePassword(req.user, req.body).then(result => {
      res.redirect("/dashboard")
   }).catch(err => {
      //error handling
      res.status(err.httpStatus || 500).send({ message: err.message, status: 0 });
   });
});


router.get('/logout', (req, res) => {
   if (req.session) {
      req.session.destroy((err) => {
         if (err) {
            return next(err);
         }
         else {
            return res.redirect('/')
         }
      })

   }

});






module.exports = router;