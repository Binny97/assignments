$(document).ready(function() {
    ForShow();
    $(".next_btn").click(function() { // Function Runs On NEXT Button Click
      let res =0;
      let divisible=$('#myform fieldset:visible').attr("id");
      if(divisible == "first")
      {
          res=validateform1();
          if(res==0)
          {
            $(this).parent().next().fadeIn('slow');
            $(this).parent().css({
            'display': 'none'
            });
          }
      }
      else if(divisible == "second")
          {
              res=validateform2();
              if(res==0)
              {
                $(this).parent().next().fadeIn('slow');
                $(this).parent().css({
                'display': 'none'
                });
              }
          }
          
    // Adding Class Active To Show Steps Forward;
    $('.active').next().addClass('active');
    });
    $(".pre_btn").click(function() { // Function Runs On PREVIOUS Button Click
    $(this).parent().prev().fadeIn('slow');
    $(this).parent().css({
    'display': 'none'
    });
    // Removing Class Active To Show Steps Backward;
    $('.active:last').removeClass('active');
    });
    // Validating All Input And Textarea Fields
    $(".submit_btn").click(function() {
      let res =0;
      let divisible=$('#myform fieldset:visible').attr("id");
      if(divisible == "third")
      {
          res=validateform3();
          if(res==0)
          {
            $(this).parent().next().fadeIn('slow');
            $(this).parent().css({
            'display': 'none'
            });
            formsubmit();
          }
         
        } 
    });
    });
    // function for validate form 
    function validateform1()
    {
      let flag = 0;
      let fname=$("#fname").val();
      let lname=$("#lname").val();
   //first name
   var letter = /^[A-Za-z]+$/;
  
    if (!fname.match(letter))
    {
      $("#Fname").text("**enter alphabat only");
      setTimeout(function(){ $("#Fname").text("");},3000);
      flag = 1;
    }
   if (fname.length < 3 || fname.length > 10)
   {
     $("#Fname").text("**please select minimum 3 letter and max 10 letter only");
     setTimeout(function(){ $("#Fname").text("");},3000);
     flag = 1;
   }
   if (fname == "")
   {
     $("#Fname").text("**name must be filled out");
     setTimeout(function(){ $("#Fname").text("");},3000);
     flag = 1;
   }
   //last name
   var letter = /^[A-Za-z]+$/;
  
    if (!lname.match(letter))
    {
      $("#Lname").text("**enter alphabat only");
      setTimeout(function(){ $("#Lname").text("");},3000);
      flag = 1;
    }
   if (lname.length < 3 || lname.length > 10)
   {
     $("#Lname").text("**please select minimum 3 letter and max 10 letter only");
     setTimeout(function(){ $("#Lname").text("");},3000);
     flag = 1;
   }
   if (lname == "")
   {
     $("#Lname").text("**name must be filled out");
     setTimeout(function(){ $("#Lname").text("");},3000);
     flag = 1;
   }
  
   return flag;
   }
   // for vlaidate form second
   function validateform2()
    {
      let flag = 0;
      let country = $("#country").val();
      let state = $("#state").val();
      let cityname = $("#city").val();
    if (country == "")
    {
      $("#Country").text("select country");
      setTimeout(function(){ $("#Country").text("");},3000);
      flag = 1;
    }
    if (state == "")
    {
      $("#States").text("select State");
      setTimeout(function(){ $("#States").text("");},3000);
      flag = 1;
    }
    if (cityname == "")
   {
     $("#City").text("**please enter your city name");
     setTimeout(function(){ $("#City").text("");},3000);
     flag = 1;
   }
   if (cityname.length < 2 || cityname.length > 30)
   {
     $("#City").text("please enter minimum 2 letter and max 30 letter only");
     setTimeout(function(){ $("#City").text("");},3000);
     flag = 1;
   }
    return flag;
    }
  
    // for vlaidate form second
   function validateform3()
   {
     let flag = 0;
     let qualification = $("#qualification").val();
     let startyear = $("#startyear").val();
     let endyear = $("#endyear").val();
     let school = $("#school").val();
   if (qualification == "")
   {
     $("#Qualification").text("select qualification");
     setTimeout(function(){ $("#Qualification").text("");},3000);
     flag = 1;
   }
   if (startyear == "")
   {
     $("#StartYear").text("please enter your start year");
     setTimeout(function(){ $("#StartYear").text("");},3000);
     flag = 1;
   }
   var year = /^(19|20)\d{2}$/;
   if (!startyear.match(year))
   {
     $("#StartYear").text("please enter valid year");
     setTimeout(function(){ $("#StartYear").text("");},3000);
     flag = 1;
   }
   if (endyear == "")
  {
    $("#EndYear").text("please enter end year");
    setTimeout(function(){ $("#EndYear").text("");},3000);
    flag = 1;
  }
  var year = /^(19|20)\d{2}$/;
   if (!endyear.match(year))
   {
     $("#EndYear").text("please enter valid year");
     setTimeout(function(){ $("#EndYear").text("");},3000);
     flag = 1;
   }
  if (school== "")
  {
    $("#SchoolName").text("please enter minimum 2 letter and max 30 letter only");
    setTimeout(function(){ $("#SchoolName").text("");},3000);
    flag = 1;
  }
  if (school.length < 2 || school.length > 30)
  {
    $("#SchoolName").text("please enter minimum 2 letter and max 30 letter only");
    setTimeout(function(){ $("#SchoolName").text("");},3000);
    flag = 1;
  }
   return flag;
   }
    // function for submit form and showing information in form
   function formsubmit()
    {
    let fname=$("#fname").val();
    let lname=$("#lname").val();
    let country=$("#country").children("option:selected").text();
    let state = $("#state").children("option:selected").text();
    let city = $("#city").val();
    let qualification = $("#qualification").val();
    let startyear = $("#startyear").val();
    let endyear = $("#endyear").val();
    let school = $("#school").val();
    
  
    $("#FirstName").text(fname);
    $("#LastName").text(lname);
    $("#CounTry").text(country);
    $("#StaTe").text(state);
    $("#CiTy").text(city);
    $("#QualiFication").text(qualification);
    $("#Start-Year").text(startyear);
    $("#End-Year").text(endyear);
    $("#School-Name").text(school);
   }
   //function for change state on country select
   function ForShow(){
    $("#country").change(function() {
      if ($(this).data('options') === undefined) {
        /*Taking an array of all options-2 and kind of embedding it on the select1*/
        $(this).data('options', $('#state option').clone());
      }
      var id = $(this).val();
      var options = $(this).data('options').filter('[name=' + id + ']');
      $('#state').html(options);
    });
  }