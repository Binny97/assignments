var async = require('async');

async.waterfall([
    function(callback) {
        console.log("in first function");
        callback(null, 'one', 'two');
        
    },
    function(arg1, arg2, callback) {
        // arg1 now equals 'one' and arg2 now equals 'two'
        console.log(arg1);
        console.log(arg2);
        console.log("in second function");
        callback(null, 'three');
        
    },
    function(arg1, callback) {
        // arg1 now equals 'three'
        console.log("in third function");
        callback(null, 'done');
        
    }
], function (err, result) {
    // result now equals 'done
    console.log("success");
});


// async.waterfall([
//     myFirstFunction,
//     mySecondFunction,
//     myLastFunction,
// ], function (err, result) {
//     // result now equals 'done'
//     console.log("success")
// });
// function myFirstFunction(callback) {
//     callback(null, 'one', 'two');
//     console.log("first function")
// }
// function mySecondFunction(arg1, arg2, callback) {
//     // arg1 now equals 'one' and arg2 now equals 'two'
//     callback(null, 'three');
//     console.log("second function")
// }
// function myLastFunction(arg1, callback) {
//     // arg1 now equals 'three'
//     callback(null, 'done');
//     console.log("third function")
// }