class Thenable {
    constructor(num) {
      this.num = num;
    }
    then(resolve, reject) {
      console.log(resolve);
      // resolve with this.num*2 after 1000ms
      setTimeout(() => resolve(this.num * 2), 1000); // (*)
    }
  };
  
  async function f() {
    // waits for 1 second, then result becomes 2
    let result = await new Thenable(1);
    console.log(result);
  }
  
  f();

//Asyn await

// async function func1(){
//     // return new Promise(function(resolve,reject){
//         setTimeout(() => {
//             const err=false;
//             if(!err){
//                 console.log("Success")
//                 return Promise.resolve();
//             }
//             else{
//                 console.log("Failed")
//                 Promise.reject();
//             }
            
//         }, 3000);
//     // })
// }

// func1().then(function(){
//     var a=1;
//     var b=2;
//      var c;
//      c=a+b;
//      console.log(c)
// }).catch(function(){
//     console.log("error")
    
// }) 

// await func1();



