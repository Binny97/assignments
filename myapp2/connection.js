var mongoose = require('mongoose');
var config = require('./config.js')
const { db: { host, port, name } } = config;
class DB {
    static connection(){
        mongoose.connect(`mongodb://${host}:${port}/${name}`,{useNewUrlParser:true,useCreateIndex: true});
var db=mongoose.connection;
db.once('open',function(){
  console.log("connected");
  console.log("db is running on port 27017");
});

db.on('error',function(){
	console.log("error");
});
        
    }
}



module.exports = DB;