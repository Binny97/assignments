$( document ).ready(function() {
    
$("#ChangePassword").validate({
    rules: {
        NewPassword: {
    required: true,
    maxlength: 10,
    minlength: 10
    },
    ConfirmPassword: {
    
    required: true,
    maxlength: 10,
    minlength: 10,
    equalTo: "#NewPassword"
    
    },
    OldPassword: {
    required: true,
    maxlength: 10,
    minlength: 10
    }
    },
    messages: {
    NewPassword: 'please enter your new password',
    ConfirmPassword: 'please confirm your password',
    OldPassword: 'please enter your previous password',
    }
    });
})
