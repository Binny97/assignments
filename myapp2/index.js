
var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var db= require('./connection.js')
var session = require('express-session');
var passport = require('passport')  
var flash = require('connect-flash');

// var indexRouter = require('./routes/index');
// var usersRouter = require('./routes/users');


var staticify = require('staticify')(path.join(__dirname, 'public'));
var app = express();var debug = require('debug')('myapp2:server');
var http = require('http');
users = [];
connections = [];
/**
 * Get port from environment and store in Express.
 */

var port = normalizePort(process.env.PORT || '3000');
app.set('port', port);

/**
 * Create HTTP server.
 */

var server = http.createServer(app);
var io = require('socket.io').listen(server); 
/**
 * Listen on provided port, on all network interfaces.
 */

server.listen(port);
server.on('error', onError);
server.on('listening', onListening);

/**
 * Normalize a port into a number, string, or false.
 */

function normalizePort(val) {
  var port = parseInt(val, 10);

  if (isNaN(port)) {
    // named pipe
    return val;
  }

  if (port >= 0) {
    // port number
    return port;
  }

  return false;
}

/**
 * Event listener for HTTP server "error" event.
 */

function onError(error) {
  if (error.syscall !== 'listen') {
    throw error;
  }

  var bind = typeof port === 'string'
    ? 'Pipe ' + port
    : 'Port ' + port;

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case 'EACCES':
      console.error(bind + ' requires elevated privileges');
      process.exit(1);
      break;
    case 'EADDRINUSE':
      console.error(bind + ' is already in use');
      process.exit(1);
      break;
    default:
      throw error;
  }
}

/**
 * Event listener for HTTP server "listening" event.
 */

function onListening() {
  var addr = server.address();
  var bind = typeof addr === 'string'
    ? 'pipe ' + addr
    : 'port ' + addr.port;
  debug('Listening on ' + bind);
}

// chat module

// io.sockets.on('connection',(socket)=>{
//   connections.push(socket);
//   console.log('connected: %s socket connected',connections.length);

//   //for Disconnect
//   socket.on('disconnect',(data)=>{
//     connections.splice(connections.indexOf(socket),1)
//     console.log('connected: %s socket connected',connections.length);

//   });
//  //send Message
//  socket.on('send message',(data)=>{

//   console.log(data)
//    io.sockets.emit('new message',{msg:data})
//   });


// });
  
// >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

// one to one chat 

io.on('connection', socket => {
  socket.on('new_user', (data, cb) => {
      console.log(data);
    if(data in users){
        cb(false);
    }
    else{
        cb(true);
        socket.nickname=data;
        users[socket.nickname]=socket;
        updateNickenames();
    }
  })
  function updateNickenames(){
      io.sockets.emit('user_name',Object.keys(users))
  }
  socket.on('send_message',(data)=>{
    var msg=data.trim();
    if(msg.substr(0,3)=== '/w '){
      console.log("hello")
      msg=msg.substr(3);
      var ind=msg.indexOf(' ');
      if(ind !== -1){
        var name=msg.substring(0, ind);
        msg=msg.substring(ind + 1);
        console.log(name);
        if(name in users){
          users[name].emit('whisper',{msg:msg,nick:socket.nickname});
          users[socket.nickname].emit('whisper',{msg:msg,nick:socket.nickname});
          console.log('whisper')
        }else{
          cb('Error! enter a valid user')
        }
      }else{
        cb('Error! enter a valid message')
      }
    }else{
      io.sockets.emit('new_message',{msg:msg,nick:socket.nickname})
    }
  });
  socket.on('disconnect', (data) => {
      if(!socket.nickname) return;
      delete users[socket.nickname];
      updateNickenames();
    });
});

// end one to one chat


// end chat

app.set('trust proxy',1);
db.connection();
var swig = require('swig')
// view engine setup
app.set('views', path.join(__dirname, 'views'));
//app.set('view engine', 'jade');
app.set('view engine', 'html');
app.engine('html', swig.renderFile);
app.use(cookieParser());
app.use(session({name:'demo',resave:false, secret: '123' }));
app.use(passport.initialize());
app.use(passport.session());

app.use(flash());
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use(express.static(path.join(__dirname, 'public')));

app.use(staticify.middleware);
app.locals = {
getVersionedPath: staticify.getVersionedPath
};  
// app.use('/', indexRouter);
// app.use('/users', usersRouter);
app.use('/', require('./App/v1/user/onboard-route'))
app.use( (req,res,next)=>{
if(req.user){
  next();
}
else{
  res.redirect('/')
}
}) 
app.use('/dashboard', require('./App/v1/dashboard/route'))
app.use('/user', require('./App/v1/user/route'))
app.use('/emailTemplate', require('./App/v1/emailTemplate/route'))

// app.use('/test', require('./App/v1/test/route'))
// app.use('/user', require('./App/v1/user/route'))

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.send(err);
});

module.exports = app;
