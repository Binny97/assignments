
const data= {
    app: {
      port: 3000
    },
    db: {
      host: 'localhost',
      port: 27017,
      name: 'user'
    }
   };
   //127.0.0.1
   module.exports = data;