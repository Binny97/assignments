var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var db= require('./connection.js')
var session = require('express-session');
var passport = require('passport')
var flash = require('connect-flash');
var port=3000;
// var indexRouter = require('./routes/index');
// var usersRouter = require('./routes/users');


var staticify = require('staticify')(path.join(__dirname, 'public'));
var app = express();
app.listen(port);
app.set('trust proxy',1);
db.connection();
var swig = require('swig')
// view engine setup
app.set('views', path.join(__dirname, 'views'));
//app.set('view engine', 'jade');
app.set('view engine', 'html');
app.engine('html', swig.renderFile);
app.use(cookieParser());
app.use(session({name:'demo',resave:false, secret: '123' }));
app.use(passport.initialize());
app.use(passport.session());

app.use(flash());
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use(express.static(path.join(__dirname, 'public')));

app.use(staticify.middleware);
app.locals = {
getVersionedPath: staticify.getVersionedPath
};  
// app.use('/', indexRouter);
// app.use('/users', usersRouter);
app.use('/', require('./App/v1/user/onboard-route'))
app.use( (req,res,next)=>{
if(req.user){
  next();
}
else{
  res.redirect('/')
}
})
app.use('/dashboard', require('./App/v1/dashboard/route'))
app.use('/user', require('./App/v1/user/route'))
// app.use('/test', require('./App/v1/test/route'))
// app.use('/user', require('./App/v1/user/route'))

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.send(err);
});

module.exports = app;
