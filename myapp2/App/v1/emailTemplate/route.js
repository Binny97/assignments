var express = require('express');
var router = express.Router();
const refEmailTemplate = require("./controller");
const objEmailTemplate = new refEmailTemplate();
const emailTemplate = require("./model.js");

const emailTemplatevalidator = require("./validate");
const validation = new emailTemplatevalidator();

router.get('/',async (req,res)=>{
    var data = await objEmailTemplate.list();
    // console.log(data)
    res.render('email_listing',{data:data})

});

router.get('/emailTemplate_form',(req,res)=>{
   
    res.render('emailTemplate_form')

});

router.post('/',validation.addValidate,validation.validateHandler,(req,res,next)=>{
   
    
    objEmailTemplate.add(req.body).then(result => {
            res.redirect('/emailTemplate');
        }).catch(err => {
          res.render('emailTemplate_form')
        });

});


router.get('/edit/:id', function(req, res, next) {
    console.log(req.params.id)
    objEmailTemplate.get_template_data(req.params.id).then(result => {
        res.render('edit_emailTemplateForm', { data:result});
      }).catch(err => {
        res.status(err.httpStatus || 500).send({ message: err.message, status: 0 });
      });
    });
    

  router.post('/edit/:id',validation.addValidate,validation.validateHandler, function(req, res, next) {
    console.log(req.params.id)
    
    objEmailTemplate.update_template_data(req.params.id,req.body).then(result => {
        console.log(result);
        res.redirect('/emailTemplate');
      }).catch(err => {
        res.status(err.httpStatus || 500).send({ message: err.message, status: 0 });
    });
   });
    


router.delete('/del/:id', function(req, res, next) {
    
    objEmailTemplate.delete_template(req.params.id).then(result => {
      res.send({message:"Success"});
    }).catch(err => {
      res.status(err.httpStatus || 500).send({ message: err.message, status: 0 });
    });
  });


  
module.exports = router;