var mongoose = require('mongoose');
var schema=mongoose.Schema;
 
var template=new schema({
    name:{
        type:String,
    },
    subject:{
        type:String,
    },
    content:{
        type:String,
    },
    isdelete:{
        type:Number,
        default:0
    },
}, { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' }});

module.exports=mongoose.model("emailTemplates",template);