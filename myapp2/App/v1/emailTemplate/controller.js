const emailTemplate = require("./model.js");


class emailtemplate {

    async add(data) {
        try {
           console.log(data)
        
           await emailTemplate.create(data);
    
           return Promise.resolve();
        
        } catch (error) {
           console.log(error)
    
        }
    
     }


   async list() {
    try {
       
        let result = await emailTemplate.find({isdelete:0});;
        console.log(result)

       return Promise.resolve(result);

    } catch (error) {
       console.log(error)

    }

 }

 async get_template_data(id) {
    try {
       let data = await emailTemplate.findOne({ _id: id });

       return Promise.resolve(data);

    } catch (error) {
       console.log(error)

    }


 }

 async update_template_data(id, template_data) {
    try {

       let data = await emailTemplate.updateOne({ _id: id }, template_data);

       return Promise.resolve(data);
    } catch (error) {
       console.log(error)

    }


 }


 async delete_template(id) {
    try {

       
       let data = await emailTemplate.updateOne({ _id: id },{isdelete:1});
       return Promise.resolve(data);
    } catch (error) {
       console.log(error)
    }


 }

}




module.exports = emailtemplate;