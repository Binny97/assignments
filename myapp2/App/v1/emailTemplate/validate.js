const { check, validationResult, query } = require('express-validator');
class UserValidator {
    validateHandler(req, res, next) {
        let errors = validationResult(req);
        if (errors.isEmpty()) {
            next();
        } else {
            res.status(400).send({ message: "Validation Errors", status: 0, errors: errors });
        }
    };

        get addValidate() {
            return [
                check('name').not().isEmpty().withMessage("Please enter name"),
                check('subject').not().isEmpty().withMessage("Please enter subject"),
                check('content').not().isEmpty().withMessage("Please enter content"),
            ]
        }

    
}

module.exports = UserValidator;