const { check, validationResult, query } = require('express-validator');
class UserValidator {
    validateHandler(req, res, next) {
        let errors = validationResult(req);
        if (errors.isEmpty()) {
            next();
        } else {
            res.status(400).send({ message: "Validation Errors", status: 0, errors: errors });
        }
    };
    get loginValidate() {
            return [
                check('username').not().isEmpty().withMessage("Please enter email"),
                check('password').not().isEmpty().withMessage("Please enter password")
            ]
        }
        get addValidate() {
            return [
                check('name').not().isEmpty().withMessage("Please enter name"),
                check('email').not().isEmpty().withMessage("Please enter email"),
                check('password').not().isEmpty().withMessage("Please enter password"),
                check('number').not().isEmpty().withMessage("Please enter number"),
                check('address').not().isEmpty().withMessage("Please enter address")
            ]
        }

        get signupValidate() {
            return [
                check('name').not().isEmpty().withMessage("Please enter name"),
                check('email').not().isEmpty().withMessage("Please enter email"),
                check('number').not().isEmpty().withMessage("Please enter number"),
            ]
        }
}

module.exports = UserValidator;