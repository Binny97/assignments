var mongoose = require('mongoose');
var schema=mongoose.Schema;
 
var Taskschema=new schema({
    name:{
        type:String,
    
       
    },
    email:{
        type:String,
        unique:true,
       
    },
    password:{
        type:String,
        
       
    },
    address:{
        type:String,
       
    },
    city:{
        type:String,
       
    },
    state:{
        type:String,
       
    },
    country:{
        type:String,
       
    },
    pin:{
        type:String,
       
    },
    
    number:{
        type:String,
       
    },
    status:{
        type:Number,
        default:1
    },
    isdelete:{
        type:Number,
        default:1
    },
    user_type:{
        type:String,
        default:'user'
    },
    email_verification:{
        type:Number,
        default:0
    },
    profile_pic:{
        type:String
    },
    profile_pic_thumb:{
        type:String
    },
    file_name:{
        type:String
    }
}, { timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' }});

module.exports=mongoose.model("user",Taskschema);