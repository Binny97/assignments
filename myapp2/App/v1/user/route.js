var express = require('express');
var router = express.Router();
const refUser = require("./controller");
const objUser = new refUser();
const uservalidator = require("./validate");
const validation = new uservalidator();
var path = require('path');
var fs = require('fs');
var pdf = require('html-pdf')
const user = require("./model.js");
var swig = require('swig');
const nodemailer = require('nodemailer');

const authToken = "32bacbbe79772a6afc9cdd825f22f602";
const accountSid = "AC0cbea583c00bd9fc55d3bf008b1991ff"
var twilio = require('twilio')(accountSid,authToken)

var tplData;

var multer = require('multer');
var storage = multer.diskStorage({
  destination:(req,file,cb) =>{
    cb(null,path.join(__dirname,'/../../../public/upload'));
  },
  filename:(req,file,cb) =>{
   cb(null,Date.now()+file.originalname)
  }
})
const upload=multer({storage: storage})



router.get('/', async function(req, res, next) {
console.log(req.query)
let list = req.query
  var data = await objUser.list(req.query);
  console.log("data")

console.log(data)
  res.render('list',{data:data , message:req.flash(),active:'user'});

  });
  

  router.get('/user_form', function(req, res, next) {
    res.render('add', { title: 'Express'  });
  });


  router.post('/',validation.addValidate,validation.validateHandler,(req, res, next) => {
    
    objUser.add(req.body).then(result => {
      req.flash('success', 'User added');
          res.redirect('/user');
      }).catch(err => {
        req.flash('error', 'Email already exist ,Please select another one');
        res.render('add',{message:req.flash()})
          // res.status(err.httpStatus || 500).send({ message: err.message, status: 0 });
      });
  });


  router.get('/edit/:id', function(req, res, next) {
    console.log(req.params.id)
      objUser.get_user_data(req.params.id).then(result => {
        res.render('edit', { data:result});
      }).catch(err => {
        res.status(err.httpStatus || 500).send({ message: err.message, status: 0 });
      });
    });
    

  router.post('/edit/:id',validation.addValidate,validation.validateHandler, function(req, res, next) {
    console.log(req.params.id)
    
      objUser.update_user_data(req.params.id,req.body).then(result => {
        console.log(result);
        req.flash('success', 'User Updated');
        res.redirect('/user');
      }).catch(err => {
        res.status(err.httpStatus || 500).send({ message: err.message, status: 0 });
    });
   });
    

  router.delete('/del/:id', function(req, res, next) {
    
      objUser.delete_user_data(req.params.id).then(result => {
        req.flash('success', 'User Deleted');
        res.send({message:"Success",status:1});
      }).catch(err => {
        res.status(err.httpStatus || 500).send({ message: err.message, status: 0 });
      });
    });
 
    router.get('/view/:id', function(req, res, next) {
      console.log(req.params.id)
        objUser.view_user_data(req.params.id).then(result => {
          res.send(result);
        }).catch(err => {
          res.status(err.httpStatus || 500).send({ message: err.message, status: 0 });
        });
      });


       router.put('/status/:id', function(req, res, next) {
        console.log(req.params.id,req.body)
          objUser.user_status(req.params.id,req.body).then(() => {
 
            res.send({status:1});
          }).catch(err => {
            res.status(err.httpStatus || 500).send({ message: err.message, status: 0 });
        });
       });
      

      //  router.get('/exporteCsv', async function(req, res, next) {
  
      //    let abc = await objUser.exporteCsv();
      //    console.log(abc);
      //    res.download()
      //   res.redirect('/user')
      //   });

      router.get('/exporteCsv', function(req, res, next) {
  
         objUser.exporteCsv().then((result) => {
//  console.log("jkljdsfghjk")
          res.download(result)
          // res.redirect('/user')
        }).catch(err => {
          res.status(err.httpStatus || 500).send({ message: err.message, status: 0 });
      });
    
       
       });
        router.get('/importeCsv', (req,res) => {
          res.render('importeCsv',{data:req.user})
          
        });
       router.post('/importeCsv',upload.single('file'),(req, res, next) => {
         console.log("HELLOOOOOO")
  console.log(req.file)
         objUser.importeCsv(req.file).then( result =>{
          res.redirect('/user')
         }).catch(err => {
          res.status(err.httpStatus || 500).send({ message: err.message, status: 0 });
      });
        
        });


 
 router.get('/getpdf',async function (req, res, next) {

  var options = { format: 'Letter' };
  pdf.create(tplData, options).toFile('./'+Date.now()+'.pdf',async function(err, res) {
       if (err) return console.log(err);
       console.log(res); // { filename: '/app/businesscard.pdf' }
       var response =res.filename;
    

     let transporter = nodemailer.createTransport({
      host: "smtp.gmail.com",
      port: 587,
      secure: false, // true for 465, false for other ports
      auth: {
         user: "sainibinny1697@gmail.com", // generated ethereal user
         pass: "abhibinny" // generated ethereal password
      }
   });

   let info = await transporter.sendMail({
      to: "sainibinny1697@gmail.com", // list of receivers
      subject: "Here is your pdf", // Subject line
      attachments:[{
        filename:response,
        contentType: 'attachment/pdf'
       }]
   });


   console.log("Message sent: %s", info.messageId);

   console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
  });

     res.redirect('/user')

    
    });


//     router.put('/pagination', async function(req, res, next) {
// console.log("heloooooooooooooooooooooooooooooo")
//         objUser.pagination(req.body).then((result)=>{
//           if(result.length==0){
//             res.send(result);
//           }
//           else{
//             var tpl = swig.compileFile('/home/brijeshwar/node project/myapp2/views/appendData.html');
//             tplData=tpl({data:result})
//             res.send(tplData)
//           }

//        })
    
//       });
      
    

    // router.get("/generatePdf/:id", async (req, res, next) => {
    //   const id = req.params.id;
    //   const documents = await user.findOne({ _id: id });
    //   // const document = documents[0];
    //   const stream = await new Promise((resolve, reject) => {
    //     pdf.create(documents).toStream((err, stream) => {
    //       if (err) {
    //         reject(reject);
    //         return;
    //       }
    //       resolve(stream);
    //     });
    //   });
    // const fileName = `${+new Date()}.pdf`;
    //   const pdfPath = `${__dirname}/home/brijeshwar/node project/myapp2/pdf/${fileName}`;
    //   stream.pipe(fs.createWriteStream(pdfPath));
    //   const doc = await models.Document.update(
    //     { pdfPath: fileName },
    //     { where: { id } }
    //   );
    //   res.json(doc);
    // });


  router.get('/twilioMsg', function(req, res, next) {
      twilio.messages.create({
        to: "+919988681556",
        from:"+18142732113",
        body:"helooooo 2020 is waiting for u"
      }).then(message => {
        console.log(message.sid)
        res.redirect('/user')
      }).catch(err => {
        res.status(err.httpStatus || 500).send({ message: err.message, status: 0 });
      });
    });

module.exports = router;