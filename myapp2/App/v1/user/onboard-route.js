var express = require('express');
var router = express.Router();
const user = require("./model.js");
const refUser = require("./controller");
const objUser = new refUser();
const uservalidator = require("./validate");
const validation = new uservalidator();
var passport = require("passport");
var LocalStrategy = require("passport-local").Strategy;


var passwordHash = require('password-hash')
//for forget password
var abc;
//


router.get('/', async function (req, res, next) {

  res.render('login',{message:req.flash()});
});

router.post('/', async function (req, res, next) {
   console.log(req.body)
   
   objUser.login(req.body).then(result =>{
      res.render('login_layout',{data:result});
   })
   
 });

router.post('/login',validation.loginValidate,validation.validateHandler,  passport.authenticate('local', {
  failuerRedirect:'/',


}),(req,res,next) =>{
  try {
         
    passport.authenticate('local', async (err, user) => {
       if (err) {
          res.redirect("/");
       }
       if (!user) {
          req.flash('error', 'wrong input , check your credentials');
          res.redirect("/");
       }
       if(user.email_verification == 0)
       {
          req.flash('error', 'your email is not verified , please verify your email');
          res.redirect("/");

       }
       else{
         req.flash('success', 'Welcome');
          res.redirect('/dashboard');
       }
       
    }
    )(req, res, next);
 } catch (err) {
    console.log("erorr in login");
    res.status(400).send({ message: err.message, status: 0 });
 }
 console.log(req.user)
});

passport.use(new LocalStrategy({ usernameField: 'username', passwordField: 'password', passReqToCallback: true },
   async (req,username, password, done) => {
      try{
         let data = await user.findOne({ email: username ,user_type:'admin'});
         
         if (!data) {
            return done(null, false);
         }
         if (!passwordHash.verify(password, data.password)) {
            return done(null, false);
         }

         return done(null, data);
      }catch(err){
         console.log("error in passport");
         console.log(err);8
      }
      

   }));

   passport.serializeUser(function (user, done) {
    done(null, user._id);
 });
 
 passport.deserializeUser( async function (id, done) {
    // users.findById(userId, (err, user) => done(err, user));
    let user_data = await user.findOne({_id: id});
    user_data = JSON.parse(JSON.stringify(user_data));
    done(null,user_data );
 });
 

router.post('/signup',validation.signupValidate,validation.validateHandler, (req, res, next) => {

  objUser.signup(req.body).then(result => {

    res.redirect('/');

  }).catch(err => {
    res.status(err.httpStatus || 500).send({ message: err.message, status: 0 });
  });
});

router.get('/email/:id', function(req, res, next) {
    objUser.user_email_status(req.params.id).then((result) => {

      res.redirect('/')
    }).catch(err => {
      res.status(err.httpStatus || 500).send({ message: err.message, status: 0 });
  });
 });

 router.post('/forgetPassword', (req, res, next) => {

   objUser.forget_password(req.body).then(result => {
 
     res.redirect('/');
 
   }).catch(err => {
     res.status(err.httpStatus || 500).send({ message: err.message, status: 0 });
   });
 });

 router.get('/form', async function (req, res, next) {

   res.render('forgetPassword_form');
 });

 router.get('/forgetPassword/:id', (req,res) => {
    console.log(req.params.id)

    abc = req.params.id;
    console.log("here is the user id")
    console.log(abc)

    res.redirect('/form')
   
 });
 
 router.post('/forgetPasswordUpdate', (req,res) => {
    console.log(abc);

   objUser.update_forgetPassword(abc,req.body).then(result => {
 
      res.redirect('/');
  
    }).catch(err => {
      res.status(err.httpStatus || 500).send({ message: err.message, status: 0 });
    });
});


router.get('/socket', async function (req, res, next) {

   res.render('chat');
 });




module.exports = router;