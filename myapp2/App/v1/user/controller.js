var mongoose = require('mongoose');
const user = require("./model.js");
const emailTemplate = require("../emailTemplate/model");

var passport = require("passport");
var LocalStrategy = require("passport-local").Strategy;
const nodemailer = require('nodemailer');
var passwordHash = require('password-hash')
var fs = require('fs');
var csv = require('fast-csv')
var path = require('path');
class User {


   async add(data) {
      try {
         console.log(data)
         let find_data = await user.findOne({ email: data.email });
         if (!find_data) {
            await user.create(data);

            return Promise.resolve();
         }
         else {
            return Promise.reject();
         }
      } catch (error) {
         console.log(error)

      }

   }
   async list(data) {
      try {
         // console.log(data,"heloooo")
         let pageNo = 1, size = 5;
         if (data.page) {
            pageNo = parseInt(data.page)
         }
         let skip = ((pageNo - 1) * size)
         let query = { user_type: 'user', isdelete: 1 }
         let userCount = await user.find(query).count();
         let totalPages = parseInt(userCount / size)
         if (userCount % size != 0) {
            totalPages++;
         }

         let result = await user.find(query).skip(skip).limit(size);
         result.current = pageNo
         if (pageNo == 1) {
            result.prev = 1
         }
         else {
            result.prev = pageNo - 1
         }
         result.next = pageNo + 1
         result.last = totalPages
         result.count = userCount
         result.sr = (pageNo - 1) * size + 1
         return Promise.resolve(result);

      } catch (error) {
         console.log(error)

      }

   }
   async get_user_data(id) {
      try {
         let data = await user.findOne({ _id: id });

         return Promise.resolve(data);

      } catch (error) {
         console.log(error)

      }


   }

   async update_user_data(id, user_data) {
      try {

         let data = await user.updateOne({ _id: id }, user_data);

         return Promise.resolve(data);
      } catch (error) {
         console.log(error)

      }


   }

   async delete_user_data(id) {
      try {


         let data = await user.updateOne({ _id: id }, { isdelete: 0 });
         return Promise.resolve(data);
      } catch (error) {
         console.log(error)
      }


   }

   async view_user_data(id) {
      try {
         let data = await user.findOne({ _id: id });
         console.log(data)
         return Promise.resolve(data);
      } catch (error) {
         console.log(error)
      }
   }

   async user_status(id, user_data) {
      try {

         let data = await user.update({ _id: mongoose.Types.ObjectId(id) }, user_data);

         return Promise.resolve();
      } catch (error) {
         console.log(error)

      }
   }


   async login(User_data) {
      try {
         let data = await user.findOne({ email: User_data.username });
         console.log(data)
         if (!data) {
            console.log("error")
         }
         else {
            return Promise.resolve(data);

         }
      } catch (error) {
         console.log(error)
      }

   }

   async signup(data) {
      try {
         console.log(data)


         var password = '';
         var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*';
         var charactersLength = characters.length;
         for (var i = 0; i < 10; i++) {
            password += characters.charAt(Math.floor(Math.random() * charactersLength));
         }
         console.log(password)
         var password_hash = passwordHash.generate(password);
         console.log(password_hash)
         data.password = password_hash
         let user_type = 'admin';
         data.user_type = user_type

         let data1 = await user.create(data);

         let emailtemplate = await emailTemplate.findOne({ "name": "SignUp email" })
         let content = emailtemplate.content
         console.log(content)
         var mapObj = {
            verifyemail: data.email,
            verifypassword: password,
            weblink: "http://localhost:3000",
            verifylink:"http://localhost:3000/email/" + data1._id
         };
         let res = content.replace(/verifyemail|verifypassword|weblink|verifylink/gi, function (matched) {
            return mapObj[matched];
         });
         let transporter = nodemailer.createTransport({
            host: "smtp.gmail.com",
            port: 587,
            secure: false, // true for 465, false for other ports
            auth: {
               user: "sainibinny1697@gmail.com", // generated ethereal user
               pass: "abhibinny" // generated ethereal password
            }
         });

         let info = await transporter.sendMail({
            to: "sainibinny1697@gmail.com", // list of receivers
            subject: "Here is your user name and password", // Subject line
            text: data.email + "\n" + password + "\n" + "http://localhost:3000" + "\n",
            // html: '<div>Email:' + data.email + '<br> Password:' + password + ' <br> Link:http://localhost:3000 <br> Please<a href="http://localhost:3000/email/' + data1._id + '"> click here</a>to verify</div>'
            // html:'<a href="http://localhost:3000/email/'+data1._id+'"> click here</a>'
            html:res
         });


         console.log("Message sent: %s", info.messageId);

         console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));

         return Promise.resolve();

      } catch (error) {
         console.log(error)

      }

   }

   async user_email_status(id) {
      try {

         let data = await user.update({ _id: id }, { $set: { email_verification: 1 } });

         return Promise.resolve(data);
      } catch (error) {
         console.log(error)

      }
   }


   async forget_password(data) {
      try {

         let user_find = await user.findOne({ email: data.email })
         console.log(user_find);

         if (user_find) {
            let transporter = nodemailer.createTransport({
               host: "smtp.gmail.com",
               port: 587,
               secure: false, // true for 465, false for other ports
               auth: {
                  user: "sainibinny1697@gmail.com", // generated ethereal user
                  pass: "abhibinny" // generated ethereal password
               }
            });

            let info = await transporter.sendMail({
               to: "sainibinny1697@gmail.com", // list of receivers
               subject: "Here is your user name and password", // Subject line
               html: '<div>Email:' + data.email + '<br><a href="http://localhost:3000/forgetPassword/' + user_find._id + '"> click here</a>to change your password.</div>'
               // html:'<a href="http://localhost:3000/email/'+data1._id+'"> click here</a>'
            });


            console.log("Message sent: %s", info.messageId);

            console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));

            return Promise.resolve();


         }


      } catch (error) {
         console.log(error)

      }

   }

   async update_forgetPassword(id, data) {
      console.log("id,data")
      console.log(id, data)
      try {
         let find_user = await user.findOne({ _id: id })
         if (find_user) {

            let password_hash = passwordHash.generate(data.password)
            let data2 = await user.update({ _id: id }, { $set: { password: password_hash } });

            return Promise.resolve(data2);
         }
      } catch (error) {
         console.log(error)

      }
   }




   // async exporteCsv() {
   //    try {

   //       const csvStream = csv.format({ headers: true});
   //       var  data = await user.find({ user_type: 'user' });
   //       data =JSON.parse(JSON.stringify(data))
   //      const ws = fs.createWriteStream(path.join(__dirname,'../../../public/csv/'+Date.now()+'.csv'))

   //      csvStream
   //      .pipe(ws)
   //      .on('end', (res)=>{
   //       //   res.download(ws.path)
   //         console.log("success");
   //       //   return Promise.resolve({filePath:filePath})
   //      });

   // data.forEach(Obj => {
   //    csvStream.write(Obj);

   //      });
   //      csvStream.end()
   //    } catch (error) {
   //       console.log(error)

   //    }
   // }


   async  exporteCsv() {
      try {
         var data = await user.find({ user_type: 'user' });
         data = JSON.parse(JSON.stringify(data))
         return new Promise(function (resolve, reject) {

            const csvStream = csv.format({ headers: true });

            var filePath = path.join(__dirname, '../../../public/csv/' + Date.now() + '.csv')

            const ws = fs.createWriteStream(filePath)

            csvStream
               .pipe(ws)
               .on('finish', (res) => {
                  console.log(ws.path)
                  resolve(ws.path);

                  console.log("success");
               })
               .on('err', (res) => {
                  console.log("error")
                  reject();
               })


            data.forEach(Obj => {
               csvStream.write(Obj);

            });
            csvStream.end()




         })

      } catch (error) {
         console.log(error)

      }
   }





   async importeCsv(file) {
      try {
         console.log("sdfgtyhujikolp;lokijhgfdfghjk");
         console.log(file);

         fs.createReadStream(file.destination + '/' + file.filename)
            .pipe(csv.parse({ headers: true }))
            .on('data', data => console.log(data, "heyyyyyyyyyyyy"))
            .on('end', data => { console.log("finish") });
         let result = await user.create(data);
         return Promise.resolve();
      } catch (error) {
         console.log(error)

      }

   }


   async pagination(file) {
      try {
         console.log(file)
         pageNo = pageNo + parseInt(file.pageNo)
         var size = parseInt(3)
         var query = {}
         if (pageNo < 0 || pageNo === 0) {
            response = { "error": true, "message": "invalid page number, should start with 1" };
            return res.json(response)
         }
         query.skip = size * (pageNo - 1)
         query.limit = size

         let data = await user.find({ user_type: 'user', isdelete: 1 }, {}, query);
         data.pageNo = (pageNo - 1) * (size + 1);

         return Promise.resolve(data);


      } catch (error) {
         console.log(error)

      }

   }


}




module.exports = User;