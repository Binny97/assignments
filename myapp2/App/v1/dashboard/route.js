var express = require('express');
var router = express.Router();
var path = require('path');
const refUser = require("./controller");
const user = require("../user/model");
const objUser = new refUser();
var paypal = require('paypal-rest-sdk');
var thumb = require("node-thumbnail")
var abc;

var multer = require('multer');
var storage = multer.diskStorage({
   destination: (req, file, cb) => {
      cb(null, path.join(__dirname, '/../../../public/upload'));
   },
   filename: (req, file, cb) => {
      cb(null, Date.now() + file.originalname)
   }
})
const upload = multer({ storage: storage })


router.get('/', (req, res) => {
   objUser.countRecord().then(result => {
      console.log(result);
      res.render('tiles', { active: 'dashboard', count: result })
   });
});


router.get('/changeProfile', (req, res) => {
   res.render('changeProfile', { data: req.user })

});

router.post('/changeProfile', (req, res) => {
   console.log("sdfghjmk")
   console.log(req.body)
   objUser.updateAdminProfile(req.user, req.body).then(result => {

      console.log(result)
      res.redirect("/dashboard")
   }).catch(err => {
      //error handling
      res.status(err.httpStatus || 500).send({ message: err.message, status: 0 });
   });
});

router.post('/profileImage', (req, res) => {
   console.log(req.body)
   objUser.uploadAdminProfileImage(req.user, req.body).then(result => {

      //  console.log(result)
      res.redirect("/dashboard")
   }).catch(err => {
      //error handling
      res.status(err.httpStatus || 500).send({ message: err.message, status: 0 });
   });
});

router.post('/changePasswordUpdate', (req, res, next) => {
   objUser.changePassword(req.user, req.body).then(result => {
      res.redirect("/dashboard")
   }).catch(err => {
      //error handling
      res.status(err.httpStatus || 500).send({ message: err.message, status: 0 });
   });
});


router.get('/logout', (req, res) => {
   if (req.session) {
      req.session.destroy((err) => {
         if (err) {
            return next(err);
         }
         else {
            return res.redirect('/')
         }
      })

   }

});


router.get('/payment', (req, res) => {

   paypal.configure({
      'mode': 'sandbox', //sandbox or live
      'client_id': 'AXQbMYwkZdVpAs0cIYuejInS5koqUri1OFsExD4NcktdvApWFHkjjsGkN47k_9ALeOcRNJ-9nku5Uoc3',

      'client_secret': 'EBu6SGzjr0J8pnNI6rGSIc4vL5oTvvxWDPQq-WkikHbHjCbAUuo9saa2A-qICo_olWqZ1QqgySDsdstR'
   });


   var create_payment_json = {
      "intent": "sale",
      "payer": {
         "payment_method": "paypal"
      },
      "redirect_urls": {
         "return_url": "http://localhost:3000/dashboard/success",
         "cancel_url": "http://localhost:3000/dashboard/cancle"
      },
      "transactions": [{
         "item_list": {
            "items": [{
               "name": "Chicken leg",
               "sku": "001",
               "price": "50.00",
               "currency": "USD",
               "quantity": 1
            }]
         },
         "amount": {
            "currency": "USD",
            "total": "50.00"
         },
         "description": "Best Quality chicken"
      }]
   };


   paypal.payment.create(create_payment_json, function (error, payment) {
      if (error) {
         throw error;
      } else {
         console.log("Create Payment Response");
         console.log(payment);
         for (let i = 0; i < payment.links.length; i++) {
            if (payment.links[i].rel == 'approval_url') {
               res.redirect(payment.links[i].href);
            }

         }
      }
   });

});



// router.get('/success',(req,res) => {

//    const payerId = req.query.payerId
//    const paymentId = req.query.paymentId

//    const execute_payment_json = {
//       "payer_id":payerId,
//       "transactions":[{
//          "amount": {
//             "currency": "USD",
//             "total": "1.00"
//         }
//       }]
//    };
//   paypal.payment.create(paymentId,execute_payment_json, function (error, payment) {
//       if (error) {
//          console.log(error.response)
//           throw error;
//       } else {
//           console.log("Create Payment Response");
//           console.log(payment);
//           res.send('success')
//       }
//   });



// });

router.get('/success', (req, res) => {
   const payerId = req.query.PayerID;
   const paymentId = req.query.paymentId;
   const execute_paymont_json = {
      "payer_id": payerId,
      "transactions": [{
         "amount": {
            "currency": "USD",
            "total": "50.00"
         }

      }]

   };
   console.log(execute_paymont_json)
   paypal.payment.execute(paymentId, execute_paymont_json, (err, payment) => {
      if (err) {
         console.log(err.response);
         throw (err);
      }
      else {
         console.log(JSON.stringify(payment))
         res.redirect('/dashboard')
      }
   })
});

router.get('/cancle', (req, res) => { res.send('cancled') });






module.exports = router;