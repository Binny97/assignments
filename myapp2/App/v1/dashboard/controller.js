var mongoose = require('mongoose');
const user = require("../user/model");
var passwordHash = require('password-hash')
var path = require('path');
var thumb = require("node-thumbnail").thumb
class dashboard {



   async countRecord(){
      let data= await user.count({"isdelete" : 1,"user_type":"user"});
      return Promise.resolve(data);
      }
      

      async changePassword(user_data,data) {

         try {
            let verify= passwordHash.verify(data.OldPassword,user_data.password)
            if(verify){

            let password_hash = passwordHash.generate(data.NewPassword)
            console.log(password_hash)
         let result = await user.update({ _id: user_data._id }, { $set: { password: password_hash } });
         return Promise.resolve(result);
            }

         } catch (err) {
             return Promise.reject({ message: err.message, httpStatus: 400 })
         }
     }


     


      async updateAdminProfile(user_data,data){
         try {
            let result = await user.updateOne({ _id:user_data._id },data);
   
            return Promise.resolve(result);
         } catch (error) {
            console.log(error)
   
         }
         }
     


      // async uploadAdminProfileImage(user_data,url){
      //    try {
      //       console.log("__dirname")
      //       console.log(path.join(__dirname,'../../../public/thumbNail/'))
      //       thumb({
      //          source: path.join(__dirname,'../../../public/upload/',url.url),
      //          destination: path.join(__dirname,'../../../public/thumbNail/')
      //        }).then( async function() {
      //          console.log('Success');
      //          let result = await user.updateOne({ _id:user_data._id },{profile_pic:url.url});
   
      //          return Promise.resolve(result);
      //        }).catch(function(e) {
      //          console.log('Error', e.toString());
      //        });
     
      //    } catch (error) {
      //       console.log(error)
   
      //    }
      //    }


      async uploadAdminProfileImage(user_data,url){
         try {
         
               let result = await user.updateOne({ _id:user_data._id },{profile_pic:url.url});
   
               return Promise.resolve(result);
            
         } catch (error) {
            console.log(error)
   
         }
         }
}




module.exports = dashboard;